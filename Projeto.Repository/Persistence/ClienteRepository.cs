﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NHibernate;
using NHibernate.Linq; //LAMBDA..
using Projeto.Entities;
using Projeto.Repository.Connections;

namespace Projeto.Repository.Persistence
{
    public class ClienteRepository
    {
        public void Insert(Cliente c)
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                ITransaction t = s.BeginTransaction(); //abrindo transação..
                s.Save(c); //gravando o cliente na base de dados..
                t.Commit(); //executando..
            }
        }
        //método para atualizar um cliente na base de dados..
        public void Update(Cliente c)
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                ITransaction t = s.BeginTransaction(); //abrindo transação..
                s.Update(c); //gravando o cliente na base de dados..
                t.Commit(); //executando..
            }
        }
        //método para excluir um cliente na base de dados..
        public void Delete(Cliente c)
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                ITransaction t = s.BeginTransaction(); //abrindo transação..
                s.Delete(c); //excluindo o cliente na base de dados..
                t.Commit(); //executando..
            }
        }
        //método para retornar todos os clientes da base de dados..
        public List<Cliente> FindAll()
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                //select * from Cliente..
                return s.Query<Cliente>().ToList();
            }
        }
        //método para buscar 1 cliente na base de dados pelo id..
        public Cliente FindById(int idCliente)
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                //select * from Cliente where IdCliente = ?
                return s.Get<Cliente>(idCliente);
            }
        }
        //método para buscar clientes pelo nome..
        public List<Cliente> FindByNome(string nome)
        {
            using (ISession s = HinernateUtil.GetSessionFactory().OpenSession())
            {
                //select * from Cliente where Nome like '%Ana%' order by Nome
                return s.Query<Cliente>()
                .Where(c => c.Nome.Contains(nome))
                .OrderBy(c => c.Nome)
                .ToList();
            }
        }
    }
}