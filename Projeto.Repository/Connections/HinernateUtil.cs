﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using Projeto.Repository.Mappings;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projeto.Repository.Connections
{
   public class HinernateUtil
    {
        public static ISessionFactory session;
        public static ISessionFactory GetSessionFactory()
        {
            //return Fluently
            //.Configure()
            //.Database(
            //MsSqlConfiguration.MsSql2008
            //.ConnectionString
            //(ConfigurationManager.ConnectionStrings
            //["teste "].ConnectionString))
            //.Mappings(m => m.FluentMappings.AddFromAssemblyOf
            //<ClienteMapping>())
            //.BuildSessionFactory();

            if (session != null)
                return session;

            FluentConfiguration _configuration = Fluently.Configure()
                                                        .Database(MySQLConfiguration
                                                        .Standard
                                                        .ConnectionString(x => x.Server("localhost")
                                                                                .Username("root")
                                                                                .Password("root")
                                                                                .Database("mysqlecomm")))
                                                                                .Mappings(
                                                                                          c => c.FluentMappings.AddFromAssemblyOf<ClienteMapping>()).ExposeConfiguration(cfg => new SchemaExport(cfg).Create(true,true)) ;

            session = _configuration.BuildSessionFactory();
            return session;                                                         
                                                                      
        }
    }
}
