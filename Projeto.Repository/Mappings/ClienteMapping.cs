﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Projeto.Entities; //classes de entidade..
using FluentNHibernate.Mapping; //mapeamento..

namespace Projeto.Repository.Mappings
{
    public class ClienteMapping : ClassMap<Cliente>
    {
        public ClienteMapping()
        {
            Table("Cliente");

            Id(c => c.IdCliente)
            .Column("IdCliente")
            .GeneratedBy
            .Identity();

            Map(c => c.Nome)
            .Column("Nome")
            .Length(50)
            .Not.Nullable();
            Map(c => c.Email)
            .Column("Email")
            .Length(50)
            .Not.Nullable();
            Map(c => c.Sexo)
            .Column("Sexo")
            .Length(15)
            .Not.Nullable();
            Map(c => c.EstadoCivil)
            .Column("EstadoCivil")
            .Length(15)
            .Not.Nullable();
        }
    }
}
