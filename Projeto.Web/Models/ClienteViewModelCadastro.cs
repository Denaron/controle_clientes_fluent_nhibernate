﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Projeto.Entities.Types; //enums..
using System.ComponentModel.DataAnnotations; //

namespace Projeto.Web.Models
{
   
        public class ClienteViewModelCadastro
        {
            [Required(ErrorMessage = "Por favor, informe o nome do cliente.")]
            [Display(Name = "Nome do Cliente:")] //label..
            public string Nome { get; set; }
            [Required(ErrorMessage = "Por favor, informe o email do cliente.")]
            [Display(Name = "Email:")] //label..
            public string Email { get; set; }
            [Required(ErrorMessage = "Por favor, selecione o sexo do cliente.")]
            [Display(Name = "Selecione o Sexo:")] //label..
            public Sexo Sexo { get; set; }
            [Required(ErrorMessage = "Por favor, selecione o estado civil do cliente.")]

            [Display(Name = "Selecione o Estado Civil:")] //label..
            public EstadoCivil EstadoCivil { get; set; }
        }
}