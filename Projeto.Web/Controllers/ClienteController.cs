﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Projeto.Entities;
using Projeto.Web.Models;
using Projeto.Repository.Persistence;

namespace Projeto.Web.Controllers
{
    public class ClienteController : Controller
    {
        // GET: Cliente
        public ActionResult Cadastro()
        {
            return View();
        }

        [HttpPost] //recebe requisição POST da página..
        public ActionResult Cadastro(ClienteViewModelCadastro model)
        {
            //verificar se os dados da model passaram nas validações..
            if (ModelState.IsValid)
            {
                try
                {
                    Cliente c = new Cliente(); //entidade..
                    c.Nome = model.Nome;
                    c.Email = model.Email;
                    c.Sexo = model.Sexo;
                    c.EstadoCivil = model.EstadoCivil;
                    ClienteRepository rep = new ClienteRepository();

                    rep.Insert(c); //cadastrando..
                    ViewBag.Mensagem = "Cliente " + c.Nome + ", cadastrado com sucesso.";
                    ModelState.Clear(); //limpar os campos..
                }
                catch (Exception e)
                {
                    //exibir mensagem de erro..
                    ViewBag.Mensagem = e.Message;
                }
            }
            return View();
        }

        public ActionResult Consulta()
        {
            //criando uma lista da classe de modelo..
            List<ClienteViewModelConsulta> lista
            = new List<ClienteViewModelConsulta>();
            try
            {
                //varrer os clientes do banco de dados..
                ClienteRepository rep = new ClienteRepository();
                foreach (Cliente c in rep.FindAll()) //consulta..
                {
                    ClienteViewModelConsulta model
                    = new ClienteViewModelConsulta();
                    model.IdCliente = c.IdCliente;
                    model.Nome = c.Nome;
                    model.Email = c.Email;
                    model.Sexo = c.Sexo.ToString();
                    model.EstadoCivil = c.EstadoCivil.ToString();
                    lista.Add(model);
                }
            }
            catch (Exception e)
            {
                //exibir mensagem de erro..
                ViewBag.Mensagem = e.Message;
            }
            return View(lista);
        }
    }
}